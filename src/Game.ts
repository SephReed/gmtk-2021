import { div, Source, Sourcify, span } from "helium-ui";
import { Canvas } from "./Canvas";
import { NPC } from "./NPC";
import { Player } from "./Player";
import { Track } from "./Track";
import { randomInRange } from "./Utility";
import { CarTypeList, Vehicle } from "./Vehicle";



export class _Game {
  public logicRate = 40;
  public canvas = new Canvas();
  public state = Sourcify({
    gameMode: "fresh" as "fresh" | "new-game" | "loss" | "win" | "playing",
    paused: false,
    update: Date.now(),
  })


  constructor() {
    document.addEventListener("mouseleave", () => this.state.paused = true);
    document.addEventListener("mouseenter", () => this.state.paused = false);
  }

  protected _debugDomNode: HTMLDivElement;
  public get debugger() {
    if (this._debugDomNode) { return this._debugDomNode; }

    const debugSource = new Source(Date.now());
    this._debugDomNode = div("Debugger", {
      style: {
        display: "inline-grid",
        gridTemplateColumns: "auto auto auto auto",
        gridColumnGap: "15px"
      }
    },() => {
      const date = debugSource.get();
      return this.vehicles.map((car) => 
        div("CarDebug", {
          style: { display: "contents" }
        },[
          span([`${car.state.name}: `]),
          span({
            style: { color: car.state.acceleration < 0 ? "red" : "green" },
            innards: `${car.state.acceleration.toFixed(2)}mph/s `,
          }),
          span({
            // style: { color: car.state.acceleration < 0 ? "red" : "green" },
            innards: `${car.velocity.toFixed(2)}mph `,
          }),
          // span({
          //   // style: { color: car.state.points < 50 ? "red" : "green" },
          //   innards: `${~~car.state.points}pts `,
          // }),
          span({
            // style: { color: car.state.points < 50 ? "red" : "green" },
            innards: `${(car.state as any).currentReaction}`,
          }),
        ])
      );
    });

    setInterval(() => {
      debugSource.set(Date.now());
    }, 500);

    return this._debugDomNode;
  }

  public vehicles: Vehicle[] = [];
  public track = new Track({
    length: .1,
    type: "nascar"
  });
  
  public start() {
    this.canvas.start();
    setInterval(() => {
      if (this.state.paused) { return; }
      if (this.state.gameMode === "new-game") { 
        const notReadyPlayer = this.vehicles.filter((it) => it.mode === "player").find((ent) => {
          return (ent as Player).state.pedal.accel !== true;
        });
        if (notReadyPlayer) { return; }
        this.state.gameMode = "playing";
      }

      this.state.update = Date.now();

      const dts = this.logicRate / 1000;
      this.track.step(dts);
      this.vehicles.forEach((car) => car.step(dts));

      this.vehicles.forEach((car, index) => {
        const next = this.vehicles[(index + 1) % this.vehicles.length];
        let dist = next.pos - car.pos;
        if (index === this.vehicles.length - 1) {
          dist += Game.track.length;
        }

        if (dist > 12/5280) { 
          return; 
        }

        if (car.state.velocity > next.state.velocity) {
          next.state.position = Math.min(next.pos, car.pos + (12/5280))

          const dVel = Math.abs(car.velocity - next.velocity);
          car.state.velocity -= dVel/3;
          next.state.velocity += dVel;

          if (dVel > 15) {
            car.state.colliding = true;
            next.state.colliding = true;
          }
  
          // car.state.health -= dVel;
          // next.state.health -= dVel;
        }
      });

      this.vehicles = this.vehicles.filter((car) => {
        const stillAlive = car.state.health > 0;
        return stillAlive;
      });

      if(this.players.length === 0) {
        Game.state.gameMode = "loss";
      }
    }, this.logicRate); 
  }


  public setVehicles(numNpcs: number, numPlayers: number) {
    let carChoices = [];
    const carTypes = Array(numNpcs).fill(null).map(() => {
      if (carChoices.length < 4) { carChoices = CarTypeList.slice(0, CarTypeList.length); }
      return carChoices.splice(~~(Math.random() * carChoices.length), 1)[0];
    })
    const cars: Vehicle[] = carTypes.map((type, index) => new NPC({
      type,
      mode: "npc",
      name: `NPC ${index+1}`,
      weight: 2500,
      position: 0,
      velocity: 5,
      maxVelocity: randomInRange(75, 110),
      tailGatingDistance: 0.5,
      reactionTime: randomInRange(25, 300),
      coastAccel: randomInRange(-8, -4),
      accel: {
        max: randomInRange(60, 80),
        min: randomInRange(10, 15),
        curveSteepness: 10
      },
      brake: {
        max: randomInRange(40, 60),
        min: randomInRange(10, 15),
        curveSteepness: 100
      },
    }));

    const playerButtons: { go: string; brake: string }[] = [
      { go: "ArrowUp", brake: "ArrowDown" },
      { go: "1", brake: "2" },
      { go: "-", brake: "=" },
    ];



    const players = playerButtons.slice(0, numPlayers).map((btns, index) => {
      return new Player({
        type: "dragon",
        mode: `player`,
        accelKey: btns.go,
        brakeKey: btns.brake,
        name: `Player ${index +1}`,
        weight: 2500,
        position: 0.0,
        velocity: 5,
        maxVelocity: 80,
        // brakeAccel: -13.6,
        // goAccel: 14.2,
        coastAccel: -4,
        // brakeAccel: -40,
        // goAccel: 20,
        accel: {
          max: randomInRange(60, 80),
          min: randomInRange(10, 15),
          curveSteepness: 10
        },
        brake: {
          max: randomInRange(40, 60),
          min: randomInRange(10, 15),
          curveSteepness: 100
        },
      })
    });

    players.forEach((player, index) => {
      const pos = ~~((index/players.length) * cars.length);
      cars.splice(pos, 0, player);
    });
    cars.forEach((veh, index) => {
      veh.state.position = Game.track.length * (index)/(cars.length)
    })

    this.vehicles = cars;  
  }

  public get players() {
    return this.vehicles.filter((it) => it.mode === "player") as Player[];
  }
}


export const Game = new _Game;