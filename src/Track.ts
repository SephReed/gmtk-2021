import { Game } from "./Game";


export class Track {
  constructor(public args: {
    length: number,
    type?: "line" | "nascar",
    shrinkRate?: number,
  }){
    args.shrinkRate = 0.001;
  }

  public get length() {
    return this.args.length;
  }

  public step(dt: number) {
    // this.args.length -= this.args.shrinkRate * dt;
  }

  public wrapPosition(pos: number): number {
    return pos % this.length;
  }

  public posToXY(pos: number) {
    let { width, height } = Game.canvas;
    const { type } = this.args;
    
    if (type === "line" || !type) {
      return {
        x: width * (Game.track.wrapPosition(pos) / this.length),
        y: 10,
        pxPerMile: width / this.length,
      }

    } else if (type === "nascar") {
      pos = Game.track.wrapPosition(pos);
      width = 700;
      const rPerMile = (4 + (2 * Math.PI)) / this.length;

      const topOffset = 205;
      const leftOffset = 12;
    
      const halfCircle = Math.PI / rPerMile;
      const straight = 2 / rPerMile;
      const stops = [
        straight,
        straight + halfCircle,
        straight + halfCircle + straight,
        straight + halfCircle + straight + halfCircle,
      ];

      const pxPerMile = width / (2 * straight);

      if (pos < stops[0]) {
        const posRatio = pos / straight;
        return {
          x: leftOffset + width/4 + (posRatio * straight * pxPerMile),
          y: topOffset,
          rotation: 90,
          pxPerMile,
        } 

      } else if (pos < stops[1]) {
        const posRatio = (pos - stops[0]) / halfCircle;
        const dy = Math.sin(Math.PI/2 - (posRatio * Math.PI));
        const dx = Math.cos(Math.PI/2 - (posRatio * Math.PI));
        const radiusPx = straight/2 * pxPerMile;

        return {
          x: leftOffset + (width * 3/4) + ((dx) * radiusPx),
          y: topOffset + ((1 - dy) * radiusPx),
          rotation: 90 + (posRatio * 180),
          pxPerMile,
        }

      } else if (pos < stops[2]) {
        const posRatio = (pos - stops[1]) / straight;
        return {
          x: leftOffset + (width * 3/4) - (posRatio * straight * pxPerMile),
          y: topOffset + (straight * pxPerMile),
          rotation: 270,
          pxPerMile
        } 

      } else if (pos < stops[3]) {
        const posRatio = (pos - stops[2]) / halfCircle;
        const dy = Math.sin((Math.PI* 3/2) - (posRatio * Math.PI));
        const dx = Math.cos((Math.PI* 3/2) - (posRatio * Math.PI));
        const radiusPx = straight/2 * pxPerMile;

        return {
          x: leftOffset + (width * 1/4) + ((dx) * radiusPx),
          y: topOffset + ((1 - dy) * radiusPx),
          rotation: 270 + (posRatio * 180),
          pxPerMile
        }
      }
    }
  }
}