import { Game } from "./Game";


export const CarTypeList = [
  // "black-stripe",
  // "black",
  "blue",
  "bus",
  // "cop",
  "dark-red",
  "dragon",
  "green",
  "grey-electric",
  "light-blue",
  "light-red",
  "lime-green",
  "old-timey-red",
  // "orange-boat",
  "pink",
  "purple",
  // "shiny-blue",
  "teal",
  "white-stripe",
  "white",
  "yellow-sport",
  "yellow"
] as const


export type AccelCurve = {
  curveSteepness: number;
  max: number;
  min: number;
}

export interface VehicleState {
  name: string;
  mode: "npc" | "player",
  type: typeof CarTypeList[number];
  // Miles per hour
  velocity?: number;
  maxVelocity?: number;
  // Miles per hour per second
  acceleration?: number;
  // Miles
  position: number;
  weight: number;
  // Acceleration when braking (negative number)
  // brakeAccel: number;
  // Acceleration when going (positive number)
  // goAccel: number;
  coastAccel: number;
  points?: number;
  brakeLoss?: number;

  colliding?: boolean;
  health?: number;
  accel?: AccelCurve;
  brake?: AccelCurve;
}



export class Vehicle {

  constructor(
    public state: VehicleState,
  ){
    state.acceleration = state.acceleration || 0;
    state.velocity = state.velocity || 0;
    state.maxVelocity = state.maxVelocity || 100;
    state.health = state.health || 100;
    state.points = state.points || 0;
    state.brakeLoss = state.brakeLoss || 0;
  }

  public step(dt: number) {
    const { colliding, acceleration, coastAccel } = this.state;
    if (acceleration && !colliding) {
      const newVelocity = this.state.velocity + acceleration * dt;
      this.state.velocity = Math.max(0, Math.min(newVelocity, this.state.maxVelocity));

      if (acceleration < coastAccel) {
        this.state.brakeLoss += (10 * dt)
      }
    }
    if (this.state.velocity) {
      this.state.position += (this.state.velocity / 3600) * dt;
    }

    const distToNext = this.distanceTill(this.getVehicleAhead());
    const timeToNext = (distToNext/this.velocity) * 3600;
    if (timeToNext < 0.5) {
      this.state.brakeLoss -= dt * Math.min(6, 1/timeToNext);
    }

    if (this.state.colliding) {
      this.state.health -= 150 * dt;
    }
  }

  public get pos() { return this.state.position; }
  public get velocity() { return this.state.velocity; }
  public get color() { return this.state.type; }
  public get name() { return this.state.name; }
  public get mode() { return this.state.mode; }

  public distanceTill(target: Vehicle): number {
    let out = Game.track.wrapPosition(target.pos - this.pos);
    if (out < 0) {
      out += Game.track.length;
    }
    return out;
  }

  public getVehicleAhead(steps: number = 1): Vehicle {
    const cars = Game.vehicles;
    const thisIndex = cars.findIndex((it) => it === this);
    const targetIndex = ((steps + thisIndex) + cars.length) % cars.length;

    return cars[targetIndex];
  }
  

  public get image() {
    return Vehicle.getImage(`./assets/cars/${this.state.type}.png`);
  }

  public static imgMap = new Map<string, HTMLImageElement>();
  public static getImage(src: string) {
    if (this.imgMap.has(src)) { return this.imgMap.get(src); }
    const out = new Image();
    out.src = src;
    this.imgMap.set(src, out);
    return out;
  }

  public get goAccel() {
    return this.getAccel(this.state.accel);
  }

  public get brakeAccel() {
    return -1 * this.getAccel({
      max: this.brakeAccelMax,
      min: this.state.brake.min,
      curveSteepness: this.state.brake.curveSteepness
    }, true);
  }

  public get brakeAccelMax() {
    return Math.min(this.state.brake.max, Math.max(this.state.brake.min, this.state.brake.max - this.state.brakeLoss));
  }

  protected getAccel(curve: AccelCurve, reversed: boolean = false) {
    const { velocity, maxVelocity } = this.state;
    const { curveSteepness, max, min } = curve;
    if (velocity >= maxVelocity) { return reversed ? max : 0; }
    const logAtTopVel = this.log(maxVelocity + 1, curveSteepness);
    let out = this.log(velocity + 1, curveSteepness) * ((max - min)/logAtTopVel);
    if (reversed !== true) {
      out = max - out;
    } else {
      out = out + min;
    }
    return out; 
  }

  public log(x: number, base: number) {
    return Math.log(x)/ Math.log(base);
  }
}
