import { Vehicle, VehicleState } from "./Vehicle";
import { randomInRange } from "./Utility";

export interface NpcState extends VehicleState {
  // Preferred distance from car in front
  tailGatingDistance: number;
  // Time when NPC should be reacting (accelerating or braking)
  // shouldHaveReactedDt?: number;
  reactionLogic?: {
    rideAss: number;
    backOff: number;
    brakeCheck: number;
    escape: number;
  };
  currentReaction?: "rideAss" | "backOff" | "brakeCheck" | "escape"
  // Average number of milliseconds it takes to react to things
  reactionTime: number;
}


export class NPC extends Vehicle {


  constructor(
    public state: NpcState
  ){
    super(state);
    state.reactionLogic = {} as any;
  }

  public step(dt: number) {
    super.step(dt);

    const nextVehicle = this.getVehicleAhead();
    const prevVehicle = this.getVehicleAhead(-1);

    const { tailGatingDistance, reactionTime, reactionLogic } = this.state;

    const { brakeAccel, goAccel } = this;

    const timeDistanceToNext = (this.distanceTill(nextVehicle)/this.velocity) * 3600;
    const timeDistanceFromPrev = (prevVehicle.distanceTill(this)/this.velocity) * 3600;
    const prevVelocity = prevVehicle.velocity;

    // If break checking, do that
    // Then escape or back off depending on which is closer
    // When escaped, either rideAss or back off

    // 10% of the time, activate brake checking mode.
    // If car behind is [far enough] behind, then deccelerate.
    // If car is [close enough], then accelerate

    // Should brake check?
    // Should escape?
    if (
      (this.velocity > 20 || reactionLogic.brakeCheck)
      && prevVelocity * 1.1 > this.velocity
      && this.getVehicleAhead(-2).velocity > 30
      // && timeDistanceFromPrev > tailGatingDistance/4
      && (goAccel * 1.1 > Math.abs(this.velocity - prevVelocity))
    ) {
      if (reactionLogic.brakeCheck || Math.random() < .01) {
        reactionLogic.brakeCheck = reactionLogic.brakeCheck || Date.now();
        reactionLogic.escape = null;
      }
    } else {
      if (this.state.currentReaction === "brakeCheck") {
        reactionLogic.escape = reactionLogic.escape || Date.now();
      }
      reactionLogic.brakeCheck = null;
    }
    

    
    // if (timeDistanceFromPrev < tailGatingDistance/4) {
    //   reactionLogic.escape = reactionLogic.escape || Date.now();
    // } else {
    //   reactionLogic.escape = null;
    // }


    // let overtakeInFourTailsTime = ((this.velocity - nextVehicle.velocity) * 1.5 * tailGatingDistance);

    // Should ride ass?
    // Should back off?
    if (
      timeDistanceToNext === Infinity
      // || nextVehicle.velocity > this.velocity
      || (this.velocity < 10 && (timeDistanceToNext > tailGatingDistance))
      || (timeDistanceToNext > tailGatingDistance * 1.25 * (this.velocity/nextVehicle.velocity))
    ) {
      reactionLogic.rideAss = reactionLogic.rideAss || Date.now();
      reactionLogic.backOff = null;
    } else {
      reactionLogic.backOff = reactionLogic.backOff || Date.now();
      reactionLogic.rideAss = null;
    }

  


    // // Should escape or back off?
    // if (this.state.currentReaction === "brakeCheck" && this.distanceTill(prevVehicle) < this.state.tailGatingDistance) {
    //   shouldEscape = true;
    // }
    // // Should back off?
    // if (this.state.currentReaction !== "brakeCheck" && this.distanceTill(nextVehicle) < this.state.tailGatingDistance) {
    //   shouldBackOff = true;
    // }
    // // Should ride ass?
    // if (this.distanceTill(nextVehicle) > tailGatingDistance || timeDistance === Infinity) {
    //   shouldRideAss = true;
    // }

    const timeSince = (date: number) => {
      if (!date) { return 0; }
      const out = Date.now() - date;
      if (out < reactionTime) { return 0; }
      return Math.max(0, out);
    }

    const rideAss = timeSince(reactionLogic.rideAss);
    const backOff = timeSince(reactionLogic.backOff);
    const brakeCheck = timeSince(reactionLogic.brakeCheck);
    const escape = timeSince(reactionLogic.escape);

    if (rideAss && backOff) { throw new Error(`no both rideAss and backOff`); }
    if (brakeCheck && escape) { throw new Error(`no both brakeCheck and escape`); }

    if (rideAss && brakeCheck) {
      this.state.currentReaction = (timeDistanceFromPrev > timeDistanceToNext) ? "rideAss" : "brakeCheck";
    } else if (escape && backOff) {
      this.state.currentReaction = (timeDistanceFromPrev > timeDistanceToNext) ? "backOff" : "escape";
    } else if (rideAss || escape) {
      if (rideAss && escape) {
        this.state.currentReaction = timeDistanceFromPrev > timeDistanceToNext ? "rideAss" : "escape";
      } else {
        this.state.currentReaction = rideAss ? "rideAss" : "escape";
      }
    } else if (backOff || brakeCheck) {
      if (backOff && brakeCheck) {
        this.state.currentReaction = timeDistanceFromPrev > timeDistanceToNext ? "backOff" : "brakeCheck";
      } else {
        this.state.currentReaction = backOff ? "backOff" : "brakeCheck";
      }
    }


    const { currentReaction } = this.state;
    if (currentReaction === "rideAss" || currentReaction === "escape") {
      this.state.acceleration = goAccel;

    } else if (currentReaction === "backOff") {
      if (backOff > 3 * reactionTime) {
        this.state.acceleration = brakeAccel;
      } else {
        this.state.acceleration = this.state.coastAccel;
      }

    } else if (currentReaction === "brakeCheck") {
      this.state.acceleration = brakeAccel;
    }
    

    // if (shouldBrakeCheck && !shouldEscape) {
    //   this.state.currentReaction === "brakeCheck"
    //   this.state.reactionTimes.brakeCheck = Date.now()
    // } else if (shouldEscape) {
    //   if (shouldBackOff) {
    //     // which is closer?
    //   }
    // } else if (shouldBackOff) {
    //   // backOff
    // } else if (shouldRideAss) {
    //   // rideAss
    // }

    // // for each thing
    // //   should do thing?
    // //     set doing
    // //     start reaction time?
    // //
    // // if need to do thing && is time to react?
    // //   do thing

    // // Update acceleration based off distance from the car in front
    // // using this car's preferred tail-gating distance and a static
    // // amount of acceleration to increase/decrease by. Also take into
    // // account reaction time.
    // if (timeDistance < tailGatingDistance || timeDistance > tailGatingDistance || timeDistance === Infinity) {

    //   if (!this.state.shouldHaveReactedDt) {
    //     this.state.shouldHaveReactedDt = Date.now()
    //   } else if (Date.now() - this.state.shouldHaveReactedDt > reactionTime + randomInRange(0, 2000)) {

    //     if (timeDistance < tailGatingDistance) {
    //       this.state.acceleration = brakeAccel;
    //     } else if (timeDistance > tailGatingDistance || (timeDistance === Infinity)) {
    //       this.state.acceleration = goAccel;
    //     }
    //     this.state.shouldHaveReactedDt = null
    //   }
    // } else {
    //     this.state.shouldHaveReactedDt = null
    // }

    // TODO: Consider panic response

  }

}
