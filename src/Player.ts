import { Vehicle, VehicleState } from "./Vehicle";

export interface PlayerState extends VehicleState {
  accelKey: string;
  brakeKey: string;
  pedal?: {
    brake: boolean;
    accel: boolean;
  }
}


export class Player extends Vehicle {

  constructor(
    public state: PlayerState
  ){
    super(state);
    
    this.state.pedal = this.state.pedal || {
      brake: false,
      accel: false,
    };
    document.addEventListener("keydown", (ev) => {
      switch (ev.key) {
        case this.state.accelKey: this.state.pedal.accel = true; break;
        case this.state.brakeKey: this.state.pedal.brake = true; break;
        default: return;
      }
      ev.preventDefault();
    });
    document.addEventListener("keyup", (ev) => {
      switch (ev.key) {
        case this.state.accelKey: this.state.pedal.accel = false; break;
        case this.state.brakeKey: this.state.pedal.brake = false; break;
        default: return;
      }
      ev.preventDefault();
    })
  }

  public step(dx: number) {
    super.step(dx);
    const { brake, accel } = this.state.pedal;
    const { coastAccel } = this.state;

    const { goAccel, brakeAccel } = this;

    

  

    if (brake && accel) {
      this.state.acceleration = goAccel + brakeAccel;
    } else if (brake) {
      this.state.acceleration = brakeAccel;
    } else if (accel) {
      this.state.acceleration = goAccel;
    } else {
      this.state.acceleration = coastAccel;
    }
  }
}
