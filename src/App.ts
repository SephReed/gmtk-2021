import { append, div, el, onDomReady } from "helium-ui";
import { Canvas } from "./Canvas";
import { CarTypeList, Vehicle } from "./Vehicle";
import { Game } from "./Game";
import { NPC } from "./NPC";
import { Player } from "./Player";
import { randomInRange } from "./Utility";


(window)["RobinHood-License for helium-sdx @ 50M/2K"] = "resolved"

const app = renderApp();

onDomReady(() => {
  append(document.body, app);
})



function renderApp() {
  // let carChoices = [];
  // const carTypes = Array(15).fill(null).map(() => {
  //   if (carChoices.length < 4) { carChoices = CarTypeList.slice(0, CarTypeList.length); }
  //   return carChoices.splice(~~(Math.random() * carChoices.length), 1)[0];
  // })
  // const npcs = carTypes.map((type, index) => new NPC({
  //   type,
  //   mode: "npc",
  //   name: `npc ${index+1}`,
  //   weight: 2500,
  //   position: Game.track.length * (index + 1)/(carTypes.length + 1),
  //   velocity: 5,
  //   maxVelocity: randomInRange(75, 110),
  //   // brakeAccel: randomInRange(-30, -15),
  //   // goAccel: randomInRange(10, 30),
  //   // tailGatingDistance: randomInRange(0.25, 1.5),
  //   tailGatingDistance: 0.5,
  //   reactionTime: randomInRange(25, 300),
  //   coastAccel: randomInRange(-8, -4),
  //   accel: {
  //     max: randomInRange(60, 80),
  //     min: randomInRange(10, 15),
  //     curveSteepness: 10
  //   },
  //   brake: {
  //     max: randomInRange(40, 60),
  //     min: randomInRange(10, 15),
  //     curveSteepness: 100
  //   },
  // }));

  // Game.vehicles = [
  //   new Player({
  //     type: "dragon",
  //     mode: "player",
  //     accelKey: "ArrowUp",
  //     brakeKey: "ArrowDown",
  //     name: "player 1",
  //     weight: 2500,
  //     position: 0.0,
  //     velocity: 5,
  //     maxVelocity: 80,
  //     // brakeAccel: -13.6,
  //     // goAccel: 14.2,
  //     coastAccel: -4,
  //     // brakeAccel: -40,
  //     // goAccel: 20,
  //     accel: {
  //       max: randomInRange(60, 80),
  //       min: randomInRange(10, 15),
  //       curveSteepness: 10
  //     },
  //     brake: {
  //       max: randomInRange(40, 60),
  //       min: randomInRange(10, 15),
  //       curveSteepness: 100
  //     },
  //   }),
  //   ...npcs
  // ];

  Game.setVehicles(15, 0);
  Game.start();


  return div("App", [
    Game.canvas.domNode,
    Game.debugger,
  ])
}
