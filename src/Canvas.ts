import "./canvas.scss";


import { div, el, Sourcify } from "helium-ui";
import { Game } from "./Game";
import { Vehicle } from "./Vehicle";
import { Player } from "./Player";



export class Canvas {
  public _domNode: HTMLDivElement;
  public canvas: HTMLCanvasElement;
  public ctx: CanvasRenderingContext2D;
  


  public get height() { return this.canvas.height; }
  public get width() { return this.canvas.width; }

  public get domNode() { 
    if (this._domNode) { return this._domNode; }
    this.canvas = el("canvas");
    this.canvas.style.zIndex = "10";
    this.canvas.style.position = "relative";
    this.canvas.width = 750;
    this.canvas.height = 600;
    this._domNode = div("CanvasHolder", [
      () => Game.state.paused && div("Pause", ["Paused"]),
      div("UI", [
        () => {
          const {gameMode} = Game.state;
          if (gameMode === "loss") {
            return div("Header", "You Lose");
          } else if (gameMode === "win") {
            return div("Header", "You Win");
          }
        },
        () => {
          const {gameMode} = Game.state;
          if (["loss", "win", "fresh"].includes(gameMode)) {
            return div("StartNew", [
              div("btn", {
                innards: "New Game",
                onPush: () => { 
                  Game.state.gameMode = "new-game";
                  Game.setVehicles(15, 1);
                }
              }),
              div("btn", {
                innards: "New 2 Player Game",
                onPush: () => { 
                  Game.state.gameMode = "new-game";
                  Game.setVehicles(15, 2);
                }
              }),
              div("btn", {
                innards: "New 3 Player Game",
                onPush: () => { 
                  Game.state.gameMode = "new-game";
                  Game.setVehicles(15, 3);
                }
              }),
              // div("btn", {
              //   innards: "New Game (All NPCs)",
              //   onPush: () => { 
              //     Game.state.gameMode = "new-game";
              //     Game.setVehicles(15, 0);
              //   }
              // }),
              // div("btn", {
              //   innards: "New Game (All NPCs Packed)",
              //   onPush: () => { 
              //     Game.state.gameMode = "new-game";
              //     Game.setVehicles(25, 0);
              //   }
              // })
            ])
          } else if (gameMode === "new-game") {
            return div("Instructions", () => {
              const players = Game.players;
              if (!players.length) {
                return div([
                  "Simulation Mode",
                  div("btn", {
                    onPush: () => Game.state.gameMode = "new-game",
                    innards: "End Simulation"
                  })
                ])
              }
              return [
                ...Game.players.map((player) => 
                  div("Instruct", `${player.name}: Press ${player.state.accelKey} to start and speed up.  Use ${player.state.brakeKey} to brake.`)
                ),
                "Try to be the last three standing.  Game will start when all players are gassing it!"
              ];
            })
          } else if (gameMode === "playing") {
            const players = Game.players;
            if (!players.length) { return null; }

            return players.map((player) => {
              return div("Player", [
                player.name + " break pads: ",
                div("BrakeGauge", () => {
                  Game.state.update;
                  let brakeMeter = player.brakeAccelMax / player.state.brake.max;
                  let color = `hsl(${~~(brakeMeter * 100)}, 75%, 50%)`;
                  if (player.state.health <= 0) {
                    color = "#888";
                    brakeMeter = 1;
                  }
                  return [
                    div("Fill", { style: { backgroundColor: color, width: `${brakeMeter * 100}%`}})
                  ]
                })
              ])
            });
          }
        }
      ]),

      div("World", {
        style: {
          position: "absolute",
          top: "0px",
          left: "0px",
          bottom: "0px",
          right: "0px",
          backgroundImage: `url("./assets/World.png")`,
          backgroundSize: "100% 100%",
        }
      }),
      this.canvas,
    ])
    this.ctx = this.canvas.getContext("2d");  
    return this._domNode; 
  }


  protected get playerMarker() { return Vehicle.getImage("./assets/PlayerMarker.png"); }
  protected get explosion() { return Vehicle.getImage("./assets/Explode.png"); }

  public draw() {
    const { ctx } = this;
    ctx.clearRect(0, 0, this.width, this.height);

    ctx.fillStyle = "red";
    ctx.save();
    Game.vehicles.forEach((car) => {
      const { pos, mode } = car;
      const xy = Game.track.posToXY(pos);

      const length = (12/5280) * xy.pxPerMile;
      const width = length / 2;

      if (mode === "player") {
        // const size = length * 0.5
        // ctx.strokeStyle = "red";
        // ctx.lineWidth = 3;
        // // ctx.strokeRect(xy.x - length/4, xy.y, length, length)
        // ctx.ellipse(xy.x, xy.y, length, length, 0, 0, 0);
        ctx.drawImage(this.playerMarker, xy.x - 10 - length/4, xy.y - 10, length + 20, length + 20);
      }

      if (car.state.colliding) {
        const size = car.state.health/100 * length + 20;
        const offsetY = (size - length)/2;
        const offsetX = (size - width)/2
        this.drawImage(this.explosion, xy.x - offsetX, xy.y - offsetY, size, size, car.state.health * 10 * Math.PI);
      } else {
        this.drawImage(car.image, xy.x, xy.y, width, length, xy.rotation);
      }
      ctx.restore();
    })
  }

  protected drawImage(image: HTMLImageElement, x: number, y: number, w: number, h: number, degrees: number){
    const {ctx} = this;
    ctx.save();
    ctx.translate(x+w/2, y+h/2);
    ctx.rotate(degrees*Math.PI/180.0);
    ctx.translate(-x-w/2, -y-h/2);
    ctx.drawImage(image, x, y, w, h);
    ctx.restore();
  }


  protected frameRequest: any;
  public start() {
    if (this.frameRequest) { return; }
    this.frameRequest = requestAnimationFrame(() => {
      this.frameRequest = undefined;
      this.draw();
      this.start();
    });
  }
}