import { fusebox } from "fuse-box";

const bundler = fusebox({
  target: "browser",
  cache: false,
  devServer: {
    httpServer: { port: 7744 }
  },
  webIndex: true,
  entry: "./src/App.ts"
});


bundler.runDev();